require 'capybara/poltergeist'
session = Capybara::Session.new(:poltergeist)

unless File.file?("sites.txt")
  File.new("sites.txt", "w")
end

sites = File.readlines('sites.txt').map(&:strip)
timeout = 60

if sites == []
  puts "Please add your sites to sites.txt"
  exit
end

sites.each do |site|
  session.visit "https://tinypng.com/analyzer?#{site}"
  begin
    Capybara.using_wait_time(timeout) { session.find('.doughnut') }
    size = session.find('.total-reduction p').text
    speed = session.find('.load-speed p').text
    puts "#{site} - #{size} - #{speed}"
  rescue Capybara::ElementNotFound => e
    puts "#{site} took to long to complete"
  end
end
